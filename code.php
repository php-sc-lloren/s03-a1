<?php

$personFullNameObj = (object)[
    'firstName' => 'Alpha',
    'middleName' => 'Kenny',
    'lastName' => 'Body'
];

class Person {
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName) {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName() {
        return "Your full name is $this->firstName $this->middleName $this->lastName";
    }
}

class Developer extends Person{
    public function printName() {
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a Developer";
    }
}

class Engineer extends Person{
    public function printName() {
        return "Your name is $this->firstName $this->middleName $this->lastName and you are an Engineer";
    }
}

$person = new Person('Senku', 'Pesos', 'Ishigami');
$developer = new Developer('John', 'Finch', 'Smith');
$engineer = new Engineer('Harold', 'Myers', 'Reese');